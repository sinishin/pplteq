import { PeopleTeqTemplatePage } from './app.po';

describe('PeopleTeq App', function() {
  let page: PeopleTeqTemplatePage;

  beforeEach(() => {
    page = new PeopleTeqTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
