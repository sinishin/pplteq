using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace PeopleTeq.EntityFrameworkCore
{
    public static class PeopleTeqDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<PeopleTeqDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<PeopleTeqDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
