﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using PeopleTeq.Configuration;
using PeopleTeq.Web;

namespace PeopleTeq.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class PeopleTeqDbContextFactory : IDesignTimeDbContextFactory<PeopleTeqDbContext>
    {
        public PeopleTeqDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<PeopleTeqDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            PeopleTeqDbContextConfigurer.Configure(builder, configuration.GetConnectionString(PeopleTeqConsts.ConnectionStringName));

            return new PeopleTeqDbContext(builder.Options);
        }
    }
}
