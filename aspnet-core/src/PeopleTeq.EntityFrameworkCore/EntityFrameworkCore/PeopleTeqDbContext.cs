﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using PeopleTeq.Authorization.Roles;
using PeopleTeq.Authorization.Users;
using PeopleTeq.MultiTenancy;
using PeopleTeq.Blogs;

namespace PeopleTeq.EntityFrameworkCore
{
    public class PeopleTeqDbContext : AbpZeroDbContext<Tenant, Role, User, PeopleTeqDbContext>
    {
        public DbSet<Blog> Blogs { set; get; }
        public DbSet<Post> Posts { set; get; }
        /* Define a DbSet for each entity of the application */

        public PeopleTeqDbContext(DbContextOptions<PeopleTeqDbContext> options)
            : base(options)
        {
        }
    }
}
