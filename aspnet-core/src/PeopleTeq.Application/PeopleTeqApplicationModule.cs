﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using PeopleTeq.Authorization;

namespace PeopleTeq
{
    [DependsOn(
        typeof(PeopleTeqCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class PeopleTeqApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<PeopleTeqAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(PeopleTeqApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
