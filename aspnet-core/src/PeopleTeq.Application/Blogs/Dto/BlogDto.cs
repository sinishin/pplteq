﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace PeopleTeq.Blogs
{
    [AutoMapFrom(typeof(Blog))]
    public class BlogDto : EntityDto
    {
        public string Name { set; get; }
        public string Description { set; get; }
    }
}