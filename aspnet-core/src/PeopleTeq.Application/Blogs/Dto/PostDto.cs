﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace PeopleTeq.Blogs
{
    [AutoMapFrom(typeof(Post))]
    public class PostDto : EntityDto
    {
        public string Title { set; get; }
        public string Text { set; get; }
        
        public virtual Blog Blog { get; set; }
        public virtual int BlogId { get; set; }
    }
}