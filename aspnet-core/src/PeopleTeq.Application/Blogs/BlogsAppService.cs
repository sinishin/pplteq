﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleTeq.Blogs
{
    public class BlogsAppService : CrudAppService<Blog, BlogDto>
    {
        public BlogsAppService(IRepository<Blog, int> repository) : base(repository)
        {
        }
    }
}
