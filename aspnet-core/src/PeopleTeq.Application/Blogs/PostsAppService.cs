﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleTeq.Blogs
{
    public class PostsAppService : CrudAppService<Post, PostDto>
    {
        public PostsAppService(IRepository<Post, int> repository) : base(repository)
        {
        }
    }
}
