﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using PeopleTeq.MultiTenancy.Dto;

namespace PeopleTeq.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

