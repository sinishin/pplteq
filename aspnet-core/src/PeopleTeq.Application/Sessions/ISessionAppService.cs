﻿using System.Threading.Tasks;
using Abp.Application.Services;
using PeopleTeq.Sessions.Dto;

namespace PeopleTeq.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
