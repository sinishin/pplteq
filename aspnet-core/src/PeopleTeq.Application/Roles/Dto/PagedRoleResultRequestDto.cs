﻿using Abp.Application.Services.Dto;

namespace PeopleTeq.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

