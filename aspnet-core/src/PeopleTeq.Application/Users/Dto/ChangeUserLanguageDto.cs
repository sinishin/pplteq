using System.ComponentModel.DataAnnotations;

namespace PeopleTeq.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}