﻿using System.Threading.Tasks;
using PeopleTeq.Configuration.Dto;

namespace PeopleTeq.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
