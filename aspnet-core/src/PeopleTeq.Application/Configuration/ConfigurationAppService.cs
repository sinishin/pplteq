﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using PeopleTeq.Configuration.Dto;

namespace PeopleTeq.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : PeopleTeqAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
