﻿using System.Threading.Tasks;
using Abp.Application.Services;
using PeopleTeq.Authorization.Accounts.Dto;

namespace PeopleTeq.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
