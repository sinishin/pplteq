﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace PeopleTeq.Localization
{
    public static class PeopleTeqLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(PeopleTeqConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(PeopleTeqLocalizationConfigurer).GetAssembly(),
                        "PeopleTeq.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
