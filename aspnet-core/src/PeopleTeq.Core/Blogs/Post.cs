﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleTeq.Blogs
{
    public class Post : Entity
    {
        public string Title { set; get; }
        public string Text { set; get; }

        [ForeignKey("BlogId")]
        public virtual Blog Blog { get; set; }
        public virtual int BlogId { get; set; }
    }
}
