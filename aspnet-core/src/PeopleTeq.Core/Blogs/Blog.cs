﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PeopleTeq.Blogs
{
    public class Blog : FullAuditedEntity
    {
        public string Name { set; get; }
        public string Description { set; get; }
    }
}
