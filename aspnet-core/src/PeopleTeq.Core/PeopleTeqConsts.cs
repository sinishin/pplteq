﻿namespace PeopleTeq
{
    public class PeopleTeqConsts
    {
        public const string LocalizationSourceName = "PeopleTeq";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
