﻿using Abp.Authorization;
using PeopleTeq.Authorization.Roles;
using PeopleTeq.Authorization.Users;

namespace PeopleTeq.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
