﻿using Abp.MultiTenancy;
using PeopleTeq.Authorization.Users;

namespace PeopleTeq.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
