using Microsoft.AspNetCore.Antiforgery;
using PeopleTeq.Controllers;

namespace PeopleTeq.Web.Host.Controllers
{
    public class AntiForgeryController : PeopleTeqControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
