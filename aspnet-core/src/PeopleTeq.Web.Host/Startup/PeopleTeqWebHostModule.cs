﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using PeopleTeq.Configuration;

namespace PeopleTeq.Web.Host.Startup
{
    [DependsOn(
       typeof(PeopleTeqWebCoreModule))]
    public class PeopleTeqWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public PeopleTeqWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(PeopleTeqWebHostModule).GetAssembly());
        }
    }
}
