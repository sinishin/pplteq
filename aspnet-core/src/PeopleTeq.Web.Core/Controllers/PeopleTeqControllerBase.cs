using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace PeopleTeq.Controllers
{
    public abstract class PeopleTeqControllerBase: AbpController
    {
        protected PeopleTeqControllerBase()
        {
            LocalizationSourceName = PeopleTeqConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
