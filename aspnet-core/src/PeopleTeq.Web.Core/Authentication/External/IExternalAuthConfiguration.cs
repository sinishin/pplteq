﻿using System.Collections.Generic;

namespace PeopleTeq.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
