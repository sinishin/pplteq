﻿using Abp.AutoMapper;
using PeopleTeq.Authentication.External;

namespace PeopleTeq.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
